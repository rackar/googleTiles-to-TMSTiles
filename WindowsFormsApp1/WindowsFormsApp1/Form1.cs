﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        string path_folder;
        string level_name;

        private void btn_openDic_Click(object sender, EventArgs e)
        {

            
                FolderBrowserDialog fbd = new FolderBrowserDialog();
              //  fbd.SelectedPath = @"D:\jsws\vuec\static\imageData\";
            fbd.SelectedPath = Properties.Settings.Default.openDic;
                if (fbd.ShowDialog() == DialogResult.OK)
                {
                Properties.Settings.Default.openDic = fbd.SelectedPath ;

                Properties.Settings.Default.Save();//使用Save方法保存更改
                StringBuilder sb1 = new StringBuilder();
                StringBuilder sb2 = new StringBuilder();
                    path_folder = fbd.SelectedPath;//定义一个字符串path_folder记录选择的文件夹的地址
                    //string path_newfolder = path_folder + "\\new_name";//定义一个字符串path_newfolder记录新文件夹的地址，"new_name"为新文件夹的名字
                    //if (!Directory.Exists(path_newfolder))//判断有没有new_name文件夹
                    //    Directory.CreateDirectory(path_newfolder);//如果没有则建立一个
                    DirectoryInfo tilesMain = new DirectoryInfo(path_folder);//
                    foreach(DirectoryInfo Level in tilesMain.GetDirectories())
                    {
                        level_name = Level.Name;
                        foreach (DirectoryInfo Xfolder in Level.GetDirectories())
                        {
                            foreach(FileInfo Ypic in Xfolder.GetFiles())
                            {
                            try { 
                                string filename = System.IO.Path.GetFileNameWithoutExtension(Ypic.FullName);
                                string newname = calcNewName(Int32.Parse(level_name), Int32.Parse(filename)).ToString();
                                string thisFolder = Ypic.DirectoryName;
                            string extension = Ypic.Extension;
                            string final = thisFolder +"\\"+ newname + extension + extension;



                                Ypic.MoveTo(final);
                                sb1.Append( thisFolder + "\\" + filename + "\r\n");
                            }
                            catch(Exception ee)
                            {
                                MessageBox.Show(ee.Message);
                            }
                            }
                        }
                    }



                foreach (DirectoryInfo Level in tilesMain.GetDirectories())
                {
                    level_name = Level.Name;
                    foreach (DirectoryInfo Xfolder in Level.GetDirectories())
                    {
                        foreach (FileInfo Ypic in Xfolder.GetFiles())
                        {
                            string filename = System.IO.Path.GetFileNameWithoutExtension(Ypic.FullName);
                            
                            string thisFolder = Ypic.DirectoryName;
                           
                            string final = thisFolder + "\\" + filename ;

                            try
                            {

                                Ypic.MoveTo(final);
                                sb2.Append("new name :" + final + "\r\n");
                            }
                            catch(Exception eee)
                            {
                                MessageBox.Show(eee.Message);
                            }
                        }
                    }
                }


                richTextBox1.Text = sb1.ToString();
                richTextBox2.Text = sb2.ToString();


                MessageBox.Show("修改完成！");
                //   DirectoryInfo di = new DirectoryInfo(path_folder);//实例化DirectoryInfo对象之后，该对象就具有当前操作的目录的相关信息






            }
            


        }

        int calcNewName(int level, int num)
        {
            int total =(int) Math.Pow(2, level);
            int newNum = total - 1 - num ;

            return newNum;
        }
    }
    }

